# -*- coding: utf-8 -*-

from odoo import models, fields, api


# class A(models.Model):
#     _name = 'student'

class B(models.Model):
    _name = 'proff'
    _description = 'New Description'
    name = fields.Char(string="Name", required=True, )


class STUDENT(models.Model):
    _name = 'student'
    _rec_name = 'name'
    _description = 'Student Profile'

    name = fields.Char(string="Name", required=True, )
    image = fields.Binary(string="Image", )
    student_id = fields.Char(string="Student ID", required=True, )
    gender = fields.Selection(string="Gender", selection=[('male', 'Male'), ('female', 'Female'), ], required=True, )
    email = fields.Char(string="Email", required=False, )
    age = fields.Integer(string="Age", required=True, )
    phone = fields.Char(string="Phone", required=True, )
    note = fields.Text(string="Note", required=False, )


class DOCTOR(models.Model):
    _name = 'doctor'
    _rec_name = 'name'
    _description = 'Doctor Profile'

    name = fields.Char(string="Name", required=True, )
    image = fields.Binary(string="Image", )
    doctor_id = fields.Char(string="Doctor ID", required=True, )
    gender = fields.Selection(string="Gender", selection=[('male', 'Male'), ('female', 'Female'), ], required=True, )
    email = fields.Char(string="Email", required=False, )
    age = fields.Integer(string="Age", required=True, )
    phone = fields.Char(string="Phone", required=True, )
    note = fields.Text(string="Note", required=False, )



class Medicine(models.Model):
    _name = 'medicine'
    _rec_name = 'name'
    _description = 'Medicines'

    name = fields.Char(string="Name", required=True, )
    medicine_id = fields.Char(string="Medicine  ID", required=False, )
    expires = new_field = fields.Date(string="Expire Date", required=False, )
    note = fields.Text(string="Note", required=False, )



class Kashf(models.Model):
    _name = 'kashf'
    _rec_name = 'kashf_id'
    _description = 'el Kashf'
    kashf_id = fields.Char(string="Name", required=True, )
    note = fields.Text(string="Note", required=False, )

    student_result_id = fields.Many2one(comodel_name="student", string="Student", required=False, )
    doctor_result_id = fields.Many2one(comodel_name="doctor", string="Doctor", required=False, )
    medicine_result_id = fields.Many2one(comodel_name="medicine", string="medicine", required=False, )



